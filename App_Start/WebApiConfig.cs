﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace plnscAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "TrnPost",
                routeTemplate: "trn/loadellipse/{id}",
                defaults: new { controller = "Ellipse", action = "Post", id = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "TrnWriteLog",
                routeTemplate: "trn/writelog/{id}",
                defaults: new { controller = "WriteLog", action = "WriteLog", id = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "PrdApi",
                routeTemplate: "api/prd/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
             );
        }
    }
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Apache.NMS;
using Apache.NMS.Util;
using Apache.NMS.ActiveMQ;

namespace plnscAPI.Controllers.Trn
{
    public class EllipseController : ApiController
    {
        public static readonly string rootFolder = @"D:\EllipseLog";
        public string logFile = @"D:\EllipseLog\log.txt";
        public string arsDebug = @"D:\ArsDebug\log.txt";
       
        public string uuidData = null;

        [HttpPost]
        public async Task<string> Post()
        {
            string result = "";
            string uuid = "";
            string writtenReq = "";

            Task<string> reqst = Request.Content.ReadAsStringAsync();
            string jsonReq = reqst.Result;

            XmlDocument xmlUuid = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq);
            XmlNodeList uuidNodeList = xmlUuid.SelectNodes("SyncCodeEntry/DataArea/MSO230_OPT1Message");
            foreach (XmlNode uuidNode in uuidNodeList)
            {
                uuidData = uuidNode["Uuid"].InnerText;
            }

            //uuid = uuidData.Trim() + CurrTime.Trim();
            uuid = uuidData.Trim();
            writtenReq = "RequestUUID=" + uuid.Trim() + " " + "JSONDATA=" + jsonReq.Trim();

            if (jsonReq == "")
            {
                result = "MESSAGE IS EMPTY";
            }
            else
            {
                if (jsonReq.Trim().Contains("MSO230_OPT1Message").Equals(true))
                {
                    XmlDocument xmlReq = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq);
                    string xmlStrReq = GetXMLAsString(xmlReq);

                    if (!File.Exists(logFile))
                    {
                        using (StreamWriter sw = File.CreateText(logFile))
                        {
                            sw.WriteLine(writtenReq);
                            sw.WriteLine(xmlStrReq);
                            sw.Close();
                        }
                    }
                    else if (File.Exists(logFile))
                    {
                        File.WriteAllText(logFile, string.Empty);

                        using (StreamWriter sw = File.AppendText(logFile))
                        {
                            sw.WriteLine(writtenReq);
                            sw.WriteLine(xmlStrReq);
                            sw.Close();
                        }
                    }

                    await Task.Run(() =>
                    {
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSO230_OPT1", xmlReq, uuid);
                    });
                }
                else
                {
                    result = "NOT MSO230 MESSAGE";
                }
            }

            return result;
        }

        public string PostMessage(string url, XmlDocument xmlReq, string UUID)
        {
            string result = "kosong";
            string logMsg = "";
            string strMessage = GetXMLAsString(xmlReq);
            string secHeader = "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " + "xmlns:ip='http://www.ventyx.com/IP/1' " + "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd' >";
            strMessage = strMessage.Replace("<SyncCodeEntry>", "<SyncCodeEntry" + " "+ secHeader.Trim());
            strMessage = "<?xml version='1.0' encoding='UTF-8'?>" + strMessage;
            //strMessage = strMessage + "<uuid>" + UUID.Trim() + "</uuid>";

            if (!File.Exists(arsDebug))
            {
                using (StreamWriter sw = File.CreateText(arsDebug))
                {
                    sw.WriteLine(strMessage);
                    sw.Close();
                }
            }
            else if (File.Exists(arsDebug))
            {
                File.WriteAllText(arsDebug, string.Empty);

                using (StreamWriter sw = File.AppendText(arsDebug))
                {
                    sw.WriteLine(strMessage);
                    sw.Close();
                }
            }

            try
            {
                Uri uri = new Uri("activemq:tcp://vip-elltrn.ellipse.plnsc.co.id:61616");
                //Uri connecturi = new Uri("activemq:tcp://vip-ellprd.ellipse.plnsc.co.id:61636");

                IConnectionFactory factory = new NMSConnectionFactory(uri);

                using (IConnection con = factory.CreateConnection())
                {
                    con.ClientId = "MSO230";
                    using (ISession session = con.CreateSession())
                    {
                        IDestination destination = SessionUtil.GetDestination(session, url);
                        using (IMessageProducer producer = session.CreateProducer(destination))
                        {
                            con.Start();
                            producer.DeliveryMode = MsgDeliveryMode.Persistent;
                            producer.RequestTimeout = TimeSpan.FromSeconds(100);
                            ITextMessage textMessage = producer.CreateTextMessage(strMessage);
                            textMessage.NMSCorrelationID = "MSO230OPT1";
                            producer.Send(textMessage);
                            logMsg = "MESSAGE SENT";
                        }
                        con.Stop();
                        Thread.Sleep(100);
                    }
                }
            } catch (Exception e)
            {
                result = e.ToString();
                logMsg = e.ToString();
            }

            bool hasMessage = false;
            if (logMsg == "MESSAGE SENT")
            {
                int counter = 0;

                do
                {
                    counter = counter + 1;
                    Thread.Sleep(1000);

                    if(counter == 100)
                    {
                        logMsg = "Time Out";
                        hasMessage = true;
                    }
                    else
                    {
                        hasMessage = ReadLog(UUID, out logMsg);
                        //hasMessage = true;
                        //logMsg = "UPLOAD SUCCESS";
                    }
                    result = logMsg.Trim();
                } while (hasMessage == false);
            }

            return result;
        }

        public string GetXMLAsString(XmlDocument XmlDoc)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tx = new XmlTextWriter(sw);
            XmlDoc.WriteTo(tx);

            string str = sw.ToString();
            return str;
        }

        public bool ReadLog(string uuid, out string message)
        {
            bool result = false;
            message = "";
            string readParam = "";

            try
            {
                readParam = "ResponseUUID=" + uuid.Trim();
                using (StreamReader file = new StreamReader(logFile))
                {
                    string line;

                    while((line = file.ReadLine()) != null)
                    {
                        if (line.Contains(readParam.Trim()))
                        {
                            int intPosUrl = line.IndexOf(readParam);
                            message = line.Substring(38).Trim();
                            result = true;
                        }
                    }
                }
            } catch (System.IO.IOException e)
            {
                message = e.ToString();
                result = true;
            }

            return result;
        }

    }
}

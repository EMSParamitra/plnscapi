﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Xml;

namespace plnscAPI.Controllers
{
    public class WriteLogController : ApiController
    {
        public string logFile = @"D:\EllipseLog\log.txt";

        [HttpPost]
        public async Task<string> WriteLog()
        {
            string result = "";
            string uploadStatus = "";
            string uuid = "";
            string responseMessage = "";

            Task<string> req = Request.Content.ReadAsStringAsync();
            string jsonReq = req.Result;

            XmlDocument xmlRequest = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "ResponseMessage");
            XmlNodeList xnList = xmlRequest.SelectNodes("ResponseMessage");
            foreach (XmlNode xn in xnList)
            {
                uploadStatus = xn["UPLOADSTATUS"].InnerText.Trim();
                uuid = xn["UUID"].InnerText;
            }

            responseMessage = "ResponseUUID=" + uuid.Trim() + " " + "UPLOAD_STATUS=" + uploadStatus.Trim();

            if (!uploadStatus.Equals(null))
            {
                await Task.Run(() =>
                {
                    if (!File.Exists(logFile))
                    {
                        using (StreamWriter sw = File.CreateText(logFile))
                        {
                            sw.WriteLine(responseMessage);
                            sw.Close();
                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.AppendText(logFile))
                        {
                            sw.WriteLine(responseMessage);
                            sw.Close();
                        }
                    }
                });
                //result = Request.CreateResponse(HttpStatusCode.OK);
                result = "ResponseUUID=" + uuid.Trim() + " " + "UPLOAD_STATUS=" + uploadStatus.Trim();
            }
            return result;
        }
    }
}
